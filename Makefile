# list of phony targets
.PHONY: all
.PHONY: clean
.PHONY: run
.PHONY: debug

# toolchain
CC := arm-none-eabi-gcc
AS := arm-none-eabi-as
LD := arm-none-eabi-ld
OBJCOPY := arm-none-eabi-objcopy

# additional tools
EMU := qemu-system-arm

all : main.bin

start.o : start.S
	$(AS) -mcpu=arm926ej-s -g start.S -o start.o

main.o : main.c
	$(CC) -c -mcpu=arm926ej-s -g main.c -o main.o

main.elf : start.o main.o main.ld
	$(LD) -T main.ld main.o start.o -o main.elf

main.bin : main.elf
	$(OBJCOPY) -O binary main.elf main.bin

clean :
	rm -f main.elf main.o main.bin start.o

run : main.bin
	$(EMU) -M versatilepb -m 128M -nographic -kernel main.bin

debug : main.bin
	$(EMU) -s -S -M versatilepb -m 128M -nographic -kernel main.bin
