# Bare metal ARM example using QEMU #
This is sample app which can be compiled and run under QEMU without any operating system. 
[Credits go here](https://balau82.wordpress.com/2010/02/28/hello-world-for-bare-metal-arm-using-qemu/)

## Prerequisite ##
Basically you need two things:

* gcc cross compiller
* qemu emulator for arm architecture

In practice, when you use Ubuntu you need following packets:

* gcc-arm-none-eabi (cross compiller)
* gdb-arm-none-eabi (for debugging, see note below)
* qemu-system-arm (emulator)

## Running sample ##
The simplest way to run simulation is to type:

    make run

## Debugging ##
This task is little more complex, first of all, you need to start qemu in "debug" mode, run

    make debug

Now, you have start qemu with your app and CPU stopped. This is example debug session

    $ arm-none-eabi-gdb
    (gdb) target remote :1234
    Remote debugging using :1234
    0x00000000 in ?? ()
    (gdb) file main.elf 
    A program is being debugged already.
    Are you sure you want to change the file? (y or n) y
    Reading symbols from main.elf...done.
	(gdb) continue
	Continuing.
	^C
	Program received signal SIGINT, Interrupt.
	_Reset () at start.S:5
	5               B .
	(gdb) info registers
	r0             0x1007c  65660
	r1             0x183    387
	r2             0xa      10
	r3             0x0      0
	r4             0x0      0
	r5             0x0      0
	r6             0x0      0
	r7             0x0      0
	r8             0x0      0
	r9             0x0      0
	r10            0x0      0
	r11            0x0      0
	r12            0x0      0
	sp             0x11090  0x11090
	lr             0x10070  65648
	pc             0x10008  0x10008 <_Reset+8>
	cpsr           0x600001d3       1610613203

## Installing arm gdb under Ubuntu ##
There is [bug](https://bugs.launchpad.net/ubuntu/+source/gdb-arm-none-eabi/+bug/1320305) reported on launchpad. If you have simillar issue try mentioned workaround.

> For those who land here like me:
> 
> Some more googling gave me this workaround.
> ```sudo apt-get -o Dpkg::Options::="--force-overwrite" install gdb-arm-none-eabi```